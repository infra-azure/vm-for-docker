#deploy public ip vmauladocker
resource "azurerm_public_ip" "pip" {
  name                = "pip-vmdocker"
  resource_group_name = var.namerg
  location            = var.location
  allocation_method   = "Dynamic"
  tags                = var.tags
}

#deploy public ip vmauladocker01
resource "azurerm_public_ip" "pip01" {
  name                = "pip-vmdocker-01"
  resource_group_name = var.namerg
  location            = var.location
  allocation_method   = "Dynamic"
  tags                = var.tags
}